# Stage 1
FROM digilabpublic.azurecr.io/node:20-alpine3.19 AS node_builder

RUN corepack enable && corepack prepare pnpm@latest --activate

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY frontend/.eslintrc.cjs frontend/.npmrc frontend/.prettierrc frontend/package.json frontend/pnpm-lock.yaml frontend/postcss.config.js frontend/svelte.config.js frontend/tailwind.config.js frontend/tsconfig.json frontend/vite.config.ts ./

RUN pnpm install

COPY frontend/static static
COPY frontend/src src

# Build the static files
ENTRYPOINT sh -c 'pnpm run dev --host 0.0.0.0 --port 8080'

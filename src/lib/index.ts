export type ClaimData = {
  id: string;
  kind: string;
  type: string;
  subject: string;
  claimant: string;
  registrationStart: string;
  registrationEnd: string | null;
  validStart: string | null;
  validEnd: string | null;
  associatedOperation: string | null;
  annotatingClaims: {
    kind: string;
    id: string;
  }[];
  data: { name: string; id: string };
};

export type ClaimsResponse = {
  data: ClaimData[];
};

export async function fetchClaims(
  claimType: string,
  registrationFrom: string,
  registrationTo: string,
  validFrom: string,
  validTo: string,
): Promise<ClaimsResponse> {
  // Load the data
  const url =
    'http://otherdevorg-claim-backend-127.0.0.1.nip.io:8080/v0/claims' +
    `?claimType=${claimType}` +
    `&registrationStart=${registrationFrom}:00Z` +
    `&registrationEnd=${registrationTo}:00Z` +
    `&validStart=${validFrom}:00Z` +
    `&validEnd=${validTo}:00Z`;

  console.log('Fetching claims from', url, registrationTo);

  const response = await fetch(url);

  if (!response.ok) {
    throw new Error(`Failed to fetch claims: ${response.statusText}`);
  }

  return await response.json();
}

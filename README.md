# Historical data visualizer

## Formal vs material data

This project uses the [D3](https://github.com/d3/d3) framework. Reasons for this choice are:

- This framework is open source, has been developed for 10+ years, and is widely adopted (100k+ stars on GitHub).
- Content is highly customizable, which is more limited with frameworks like [https://github.com/chartjs/Chart.js].
- The bundle size of this framework is relatively OK, smaller than e.g. that of [Apache ECharts](https://github.com/apache/echarts).

## Developing

Install the dependencies once with `pnpm install`. Then, start a development server:

```bash
pnpm dev
```

## Building

To create a production version of your app:

```bash
pnpm build
```

You can preview the production build with `pnpm preview`.

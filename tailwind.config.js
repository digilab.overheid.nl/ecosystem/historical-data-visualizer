/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  safelist: [],
  theme: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
};
